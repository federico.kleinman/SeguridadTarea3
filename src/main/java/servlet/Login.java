package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dominio.Usuario;
import negocio.LoginRN;

public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public Login() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Usuario usuarioRequest = obtenerUsuarioDeRequest(request);
		
		LoginRN lrn = new LoginRN();
		boolean isUser = lrn.verificarUsuario(usuarioRequest);
		
		if(isUser){
			response.getWriter().write("USUARIO LOGUEADO");
		}
		else{
			response.getWriter().write("USUARIO NO LOGUEADO");
		}
	}

	private Usuario obtenerUsuarioDeRequest(HttpServletRequest request) {
		String nombre = String.valueOf(request.getParameter("nombreUsuario"));
		String pass = String.valueOf(request.getParameter("password"));
		
		Usuario user = new Usuario();
		user.setNombre(nombre);
		user.setPassword(pass);
		return user;
		
	}

}
