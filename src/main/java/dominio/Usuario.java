package dominio;

public class Usuario {

	public Usuario() {
		super();
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getCedula() {
		return cedula;
	}
	public void setCedula(String cedula) {
		this.cedula = cedula;
	}
	public String getClave_publica() {
		return clave_publica;
	}
	public void setClave_publica(String clave_publica) {
		this.clave_publica = clave_publica;
	}
	String nombre;
	String password;
	String cedula;
	String clave_publica;
	
}
